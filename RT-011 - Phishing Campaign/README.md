# Red Team Attack Operation RT-011 - Phishing - Fake Laptop Upgrade

<table>
  <tr>
    <th colspan="2"><b>Attack Operation RT011 - Phishing - Fake Laptop Upgrade</b></th>
  </tr>
  <tr>
    <td><b>Adversary</b></td>
    <td>Attacker targeting GitLab employees with the goal of obtaining credentials</td>
  </tr>
  <tr>
    <td><b>Scope</b></td>
    <td>All GitLab team members</td>
    </td>
  </tr>
  <tr>
    <td><b>Goals</b></td>
    <td>
        - Trick a sampling of GitLab team members into exposing their GitLab.com credential<br>
        - Do not actually capture passwords - username only<br>
        - Increase awareness of phishing techniques and how to identify a potential phish</br>
    </td>
  </tr>
  <tr>
    <td><b>Outcomes</b></td>
    <td>
        - A random sampling of 50 GitLab team members was selected<br>
        - 17/50 (34%) of targets clicked on the phishing link in the email<br>
        - 10/17 (59%) exposed their GitLab.com credential<br>
        - 6/50 (12%) reported the email as suspicious to SecOps</br>
    </td>
  </tr>
</table>

## Overview
The intent of this exercise was to emulate a targeted phishing (aka spear phishing) campaign against GitLab team members with the intent of capturing GitLab.com credentials. For this exercise, additional defences such as multi-factor authentication were not considered or part of the test. This phishing exercise would be considered a basic phishing attack concentrating on primary authentication credentials via a fake login page.

For this exercise Red Team obtained the domain name gitlab.company and configured it to use GSuite to facilitiate the delivery of the phishing email. The domain name and accompanying GSuite services were setup following best practices such as configuring email to use DKIM and obtaining legitimate SSL certificates. This helped the domain look less suspicious to automted phishing site detection and to human inspection. This legitimate looking infrastructure can be setup by an attacker very cheaply and in some cases for free. 

The phishing framework we leveraged for this exercise is an open source project known as GoPhish. We hosted the tool on a small linux system in our GCP infrastructure. GoPhish provides a flexible framework that is highly customizable and has built in capabilities to track and capture responses to phishing campaigns. 

## Attack Narrative
For this exercise we randomly selected 50 GitLab team members as targets of the phishing email. The email, screenshot below, was designed to appear to be a legitimate laptop upgrade offer from GitLab's IT department. Targets were asked to click on a link in order to accept their upgrade and this link was instead a fake GitLab.com login page hosted on the domain "gitlab.company". While an attacker would be able to easily capture both the username and password entered into the fake site, the Red Team determined that only capturing email addresses or login names was necessary for this exercise.

![Graphs](./data/RedTeamPhishingGraphs.png)

Of the 50 phishing emails delivered 17 recipients cicked on the provided link. Of those 17 recipients, 10 of them attempted to authenticate to the fake site. Only 6 recipients reported the suspicious email to SecOps. Those that entered their credentials were then redirected to the [GitLab Handbook](https://about.gitlab.com/handbook/security/#phishing-tests).

### Phishing Email & Website Samples

![Phishing Email](./data/RedTeamPhishEmailMay2020.png)

![EmailDetails1](./data/EmailDetails1.png)

![EmailDetails2](./data/EmailDetails2.png)

Targets could have identified that this was in fact a phishing email the following ways:

  - The email address was it-ops@gitlab.company - not a legitimate gitlab.com one. Similar-sounding domain names are a common technique used in targeted phishing campaigns.
  - The email references an older model of Macbook Pro than what most users already have. Subtle factual errors are often indicators of an illegitimate source.
  - No secondary communication method, such as Slack or a company call, provided an announcement regarding any laptop upgrades.
  - Email message header details in Gmail can be viewed (Open the message, then go to the More option in the upper right, then choose the Show Original option) to give specific clues as to the methods by which the email was generated.  Keywords such as "phish" and multiple references to the illegitimate top level domain gitlab.company are key indicators.


![Phishing Site](./data/RedTeamPhishingSiteMay2020.png)

The website could have been considered suspicious based on the following  
  - Once the link is clicked the URL displayed in the browser bar is https://gitlab.company/xxxxxx.
  - The request to login should also be considered suspicious if the target is already logged into GitLab.com.
  - Some users may have noticed a change to their usual method of authenticating using Okta's SSO.


## Recommendations
Users should be encouraged to review the following handbook entry - [GitLab Handbook](https://about.gitlab.com/handbook/security/#phishing-tests).

Due to the low number of reports of this phish Security Team should communicate to all GitLab team members on a more frequent basis about phishing attacks and what to do if one is suspected.

On a quarterly basis additional phishing exercises should be conducted targeting a different sample group.


